# Some tools I made for digital signal processing

## Plotter 

just run `plotter.py` script like this : 

```
python plotter.py WAVEFILE.wav PLOT.png 
``` 

and it will make you a png plot out of that wavefile. 

### Result 

This is a C note on a Kalimba, plotted : 

![C3 - Kalimba](./kalimba.png) 

## Wav2CSV 

just run `wav2csv` script like this : 

```
python wav2csv.py WAVEFILE.wav CSVFILE.csv 
``` 

and it will convert the audio data to CSV. 
